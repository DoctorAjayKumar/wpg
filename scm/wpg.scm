;;-------------------------------------------------------------------
;; Primitive constructors
;;-------------------------------------------------------------------

(define (pt x y)
  "construct a point"
  `(pt ,x ,y))



(define (ln a b c)
  "construct a line in standard form"
  (cond [(and (equal? a 0) (equal? b 0))
         (error "a and b cannot both be 0")]
        [(equal? a 0)
         `(ln 0 1 ,(/ c b))]
        [else
         `(ln 1 ,(/ b a) ,(/ c a))]))


;;-------------------------------------------------------------------
;; accessors
;;-------------------------------------------------------------------

(define (coord-x point)
  "x coordinate of a point"
  (list-ref point 1))



(define (coord-y point)
  "y coordinate of a point"
  (list-ref point 2))



(define (coord-a line)
  "a coordinate of a line"
  (list-ref line 1))



(define (coord-b line)
  "b coordinate of a line"
  (list-ref line 2))



(define (coord-c line)
  "c coordinate of a line"
  (list-ref line 3))



(define (is-point? point)
  "test if is a point"
  (and (equal? 'pt (list-ref point 0))
       (equal? 3 (length point))))



(define (is-line? line)
  "test if is a line"
  (and (equal? 'ln (list-ref line 0))
       (equal? 4 (length line))))



;;-------------------------------------------------------------------
;; conveniences
;;-------------------------------------------------------------------

(define origin (pt 0 0))



(define (slope-intercept slope intercept)
  "construct a line y = M*x + B where M and B are the arguments"
  ;; y = Mx + B
  ;; Mx - y + B = 0
  (ln slope -1 intercept))



(define (vline x-intercept)
  "vertical line with a given x-intercept"
  ;; x = XIntercept
  ;; 1*x + 0*y - XIntercept = 0
  (ln 1 0 (* -1 x-intercept)))



(define (hline y-intercept)
  "horizontal line with a given y-intercept"
  ; y = Y
  ; 0*x + 1*y - Y = 0
  (ln 0 1 (* -1 y-intercept)))



;;-------------------------------------------------------------------

;; Primitive operations: meet and join
;;-------------------------------------------------------------------

(define (join pt1 pt2)
  "join two distinct points to form a line"
  (define (really-join rpt1 rpt2)
    "join two points assumed to be distinct"
    ;; The answer is:
    ;;
    ;;   ln(Y1 - Y2, X2 - X1, X1Y2 - X2Y1).
    ;;
    ;; Is incident with {X1, Y1}:
    ;;
    ;;     X1*(Y1 - Y2) + Y1(X2 - X1) + (X1Y2 - X2Y1)
    ;;   = X1Y1 - X1Y2 + Y1X2 - Y1X1 + X1Y2 - X2Y1
    ;;   = 0
    ;;
    ;; Is incident with {X2, Y2}:
    ;;
    ;;     X2*(Y1 - Y2) + Y2(X2 - X1) + (X1Y2 - X2Y1)
    ;;   = X2Y1 - X2Y2 + X2Y2 - X1Y2 + X1Y2 - X2Y1
    ;;   = 0
    ;;
    ;; Given two distinct points, there is exactly one line incident
    ;; with both of them
    (define x1 (coord-x rpt1))
    (define y1 (coord-y rpt1))
    (define x2 (coord-x rpt2))
    (define y2 (coord-y rpt2))
    (define a (- y1 y2))
    (define b (- x2 x1))
    (define c (- (* x1 y2) (* x2 y1)))
    (ln a b c))
  (if (equal? pt1 pt2)
      (error "cannot join equal points")
      (really-join pt1 pt2)))



(define (meet ln1 ln2)
  "find the meet of two non-parallel lines"
  (define (really-meet rln1 rln2)
    "find the meet of two lines assumed to not be parallel"
    ;; We are solving the matrix equation
    ;;
    ;;   [A1 B1][X] = [-C1]
    ;;   [A2 B2][Y]   [-C2]
    ;;
    ;; The answer will be the inverse of the matrix, times [-C1 -C2]^t
    ;;
    ;; The inverse of the matrix is
    ;;
    ;;  (1 / (A1B2 - B1A2)) * [ B2 -B1]
    ;;                        [-A2  A1]
    ;;
    ;; So when the dust settles, we get
    ;;
    ;;   X = ( B1*C2 - B2*C1)/Det
    ;;   Y = (-A1*C2 + A2*C1)/Det
    (define a1 (coord-a rln1))
    (define b1 (coord-b rln1))
    (define a2 (coord-a rln2))
    (define b2 (coord-b rln2))
    (define c1 (coord-c rln1))
    (define c2 (coord-c rln2))
    (define det (- (* a1 b2) (* a2 b1)))
    (define x (/ (- (* b1 c2) (* b2 c1))
                 det))
    (define y (/ (- (* a2 c1) (* a1 c2))
                 det))
    (pt x y))
  (if (parallel? ln1 ln2)
      (error "cannot meet parallel lines")
      (really-meet ln1 ln2)))



;;;--------------------------------------------------------------------
;;; tests
;;;--------------------------------------------------------------------

(define (incident? pl1 pl2)
  "test incidence between a point and a line; crashes unless one argument is point and the other is a line"
  (define (ipl point line)
    "incidence between a point and a line"
    (define x (coord-x point))
    (define y (coord-y point))
    (define a (coord-a line))
    (define b (coord-b line))
    (define c (coord-c line))
    (define dot-product (+ (* a x) (* b y) c))
    (equal? 0 dot-product))
  (cond [(and (is-point? pl1) (is-line? pl2))
         (ipl pl1 pl2)]
        [(and (is-line? pl1) (is-point? pl2))
         (ipl pl2 pl1)]
        [else
         (error "incidence must be between a point and a line")]))



(define (parallel? ln1 ln2)
  "test if two lines are parallel"
  (equal? 0 (spread ln1 ln2)))



(define (perpendicular? ln1 ln2)
  "test if two lines are perpendicular"
  (equal? 1 (spread ln1 ln2)))



;;-------------------------------------------------------------------
;; statistics: quadrance and spread
;;-------------------------------------------------------------------

(define (quadrance-btwn p1 p2)
  "quadrance between two points"
  (define x1 (coord-x p1))
  (define y1 (coord-y p1))
  (define x2 (coord-x p2))
  (define y2 (coord-y p2))
  (+ (expt (- x1 x2) 2)
     (expt (- y1 y2) 2)))



(define (quadrance-of p)
  "quadrance of a point"
  (quadrance-btwn origin p))



(define (spread ln1 ln2)
  "spread between two lines ('sine squared' according to the CIA)"
  (- 1 (cross ln1 ln2)))



(define (cross ln1 ln2)
  "cross between two lines ('cosine squared' according to the CIA)"
  ;; start with the law of cosines from CIA trigonometry
  ;;
  ;; V.W = |V||W|cos(Theta)
  ;; cos(Theta) =   V . W
  ;;              ---------
  ;;               |V| |W|
  ;;
  ;; If we square this, we get the "cross"
  ;; So the cross is the dot squared divided by the product
  ;; of the quadrances
  (define a1 (coord-a ln1))
  (define b1 (coord-b ln1))
  (define a2 (coord-a ln2))
  (define b2 (coord-b ln2))
  (define dot (+ (* a1 a2) (* b1 b2)))
  (define q1  (+ (* a1 a1) (* b1 b1)))
  (define q2  (+ (* a2 a2) (* b2 b2)))
  (/ (* dot dot)
     (*  q1  q2)))
