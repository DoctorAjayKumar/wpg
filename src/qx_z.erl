% @doc qx_z = Qanal Xanal Zahlen (= integers)
-module(qx_z).

-export_type([
    z/0,
    znn/0,
    zp/0
]).
-export([
    gcd/1,
    gcd/2,
    lcm/1,
    lcm/2,
    modulo/2
]).

-type z()   :: integer().
-type znn() :: non_neg_integer().
-type zp()  :: pos_integer().

%%% API

-spec gcd(Ints :: [z()]) -> GCD :: znn().
% @doc gcd of a list of integers

gcd(List) ->
    gcd_accum(List, 0).



-spec gcd(z(), z()) -> znn().
% @doc greatest common divisor of two integers
% @end

% make sure both arguments are non-negative, and the one on the left
% is bigger
gcd(X, Y) when abs(X) < abs(Y) ->
    gcdplus(abs(Y), abs(X));
gcd(X, Y) ->
    gcdplus(abs(X), abs(Y)).



-spec lcm([z()]) -> z().
% @doc
% lcm of the empty list is 1
% otherwise lcm behaves normally
% @end

lcm(Zs) ->
    lcm_accum(Zs, 1).



-spec lcm(z(), z()) -> z().
% @doc
% the lcm of two integers is their product divided by their gcd
%
% More theoretically, the lcm is in some way the opposite of the gcd.
% If you form a directed graph of the integers ordered by
% divisibility, gcd(A, B) is the biggest node in the graph that is
% both comparable to, and (smaller than or equal to) both A and B.
% Conversely, the lcm(A, B), is the smallest node in the graph that
% is both comparable to, and (bigger than or equal to) both A and B.
% @end

lcm(Z1, Z2) ->
    (Z1 * Z2) div gcd(Z1, Z2).



-spec modulo(A :: z(), B :: zp()) -> AModB :: znn().
% @doc Like the rem operator, except this always returns a result
% within 0..B
%
% for simplicity/obviously-correct-behavior, B must be strictly
% positive

modulo(A, B) when is_integer(A),
                  is_integer(B),
                  0 < B ->
    ARemB = A rem B,
    % one of two cases
    %
    %   -B < ARemB < 0 ->
    %       % this will happen when A < 0
    %       return ARemB + B
    %   0 =< ARemB < B ->
    %       % this will happen when 0 =< A
    %       return ARemB
    NegRem = ((-1 * B) < ARemB) andalso (ARemB < 0),
    PosRem =       (0 =< ARemB) andalso (ARemB < B),
    if NegRem ->
           ARemB + B;
       PosRem ->
           ARemB
    end.


%%% INTERNALS

-spec gcd_accum(Ints :: [z()], AccumGCD :: znn()) -> GCD :: znn().
% @private
% gcd of a list of integers, with an accumulator. Should set
% accumulator to 0 on initial call, because that's the "identity
% gcd", so to speak:
%
%       gcd(X, 0) -> X;
%       gcd(0, X) -> X;

% once the accumulator is 1, shortcut
gcd_accum(_List, _GCDAccum = 1) ->
    1;
% end of the list, return the accumulated GCD
gcd_accum([], GCDAccum) ->
    GCDAccum;
% new item in the list, gcd it against the accum, move along
gcd_accum([X | Xs], GCDAccum) ->
    NewGCDAccum = gcd(X, GCDAccum),
    gcd_accum(Xs, NewGCDAccum).



-spec gcdplus(znn(), znn()) -> znn().
% @private gcd where the arguments can be assumed to be non-negative,
% and in the correct order

gcdplus(X, 0) ->
    X;
% X might be equal to Y here, in which case, modulo(X, Y) = 0, and
% the next call will degenerate into the base case
gcdplus(X, Y) when X >= Y ->
    % X = qY + R
    % R = X - qY
    % 0 =< R < Y
    % therefore any common divisor of X and Y (such as the gcd) also
    % divides R
    % therefore
    % gcd(X, Y) ->
    %   gcd(Y, R)
    gcdplus(Y, modulo(X, Y)).



-spec lcm_accum([z()], z()) -> z().
% @private
% lcm of list of integers with an accumulator
% @end

lcm_accum([], Accum) ->
    Accum;
lcm_accum([Z | Zs], Accum) ->
    lcm_accum(Zs, lcm(Z, Accum)).
