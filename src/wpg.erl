% @doc
% Basic points and lines geometry
%
% Only the basics are covered here. There's a lot more we could do,
% but we will put that off for later.
%
% WPG = Wildberger's planar geometry
% @end
-module(wpg).

-export_type([
    ln/0,
    pt/0
]).
-export([
    ln/3,
    pt/2,

    origin/0,
    one_zero/0,
    zero_one/0,
    slope_intercept/2,
    vline/1,
    hline/1,

    join/2,
    meet/2,

    incident/2,
    parallel/2,
    perpendicular/2,

    quadrance/2,
    quadrance/1,
    spread/2,
    cross/2
]).

-record(pt,
        {x :: q(),
         y :: q()}).

-record(ln,
        {cx :: q(),
         cy :: q(),
         cc :: q()}).

-type ln() :: #ln{}.
-type pt() :: #pt{}.
-type q()  :: qx_q:q().



%====================================================================
% API
%====================================================================

%--------------------------------------------------------------------
% PRIMITIVE POINT AND LINE CONSTRUCTORS
%--------------------------------------------------------------------

-spec ln(q(), q(), q()) -> ln().
% @doc
% Construct a line and reduce into canonical form
%
%
% Our canonical form is defined as {ln, A, B, C} which represents the
% equation
%
%   Ax + By + C = 0
%
% We have a scale-invariance issue once again, like with rationals,
% and this is how we resolve it
%
%   - at least one of A and B must be nonzero
%   - the first nonzero entry must be normalized to one
% @end

% At least one of A and B must be nonzero
ln({q, 0, 1}, {q, 0, 1}, _) ->
    error(badarg);
% If A = 0, then normalize by B
ln(_A = Zero = {q, 0, 1}, B, C) ->
    NewA = Zero,
    NewB = qx_q:one(),
    NewC = qx_q:divide(C, B),
    {ln, NewA, NewB, NewC};
% Otherwise, normalize by A
ln(A, B, C) ->
    NewA = qx_q:one(),
    NewB = qx_q:divide(B, A),
    NewC = qx_q:divide(C, A),
    {ln, NewA, NewB, NewC}.



-spec pt(q(), q()) -> pt().
% @doc
% just a tuple constructor

pt(X, Y) ->
    {pt, X, Y}.


%--------------------------------------------------------------------
% CONVENIENT POINT AND LINE CONSTRUCTORS
%--------------------------------------------------------------------

-spec origin() -> pt().
% @doc
% the point {0, 0}

origin() ->
    Zero = qx_q:zero(),
    pt(Zero, Zero).



-spec one_zero() -> pt().
% @doc
% the point {1, 0}

one_zero() ->
    One = qx_q:one(),
    Zero = qx_q:zero(),
    pt(One, Zero).



-spec zero_one() -> pt().
% @doc
% the point {0, 1}

zero_one() ->
    Zero = qx_q:zero(),
    One = qx_q:one(),
    pt(Zero, One).



-spec slope_intercept({slope, q()}, {y_intercept, q()}) -> ln().
% @doc
% Construct a line in slope-intercept form
%
% y = M*x + B ->
%   M*x - y + B = 0 ->
%       ln(M, -1, B).

slope_intercept({slope, M}, {y_intercept, B}) ->
    One = qx_q:one(),
    MinusOne = qx_q:minus(One),
    ln(M, MinusOne, B).



-spec vline({x_equals, X :: q()}) -> ln().
% @doc
% Construct a vertical line x = X
%
% ln(1, 0, -X)

vline({x_equals, X}) ->
    One = qx_q:one(),
    Zero = qx_q:zero(),
    MinusX = qx_q:minus(X),
    ln(One, Zero, MinusX).



-spec hline({y_equals, Y :: q()}) -> ln().
% @doc
% Construct a horizontal line y = Y.

hline({y_equals, Y}) ->
    slope_intercept({slope, zero()}, {y_intercept, Y}).



%--------------------------------------------------------------------
% PRIMITIVE OPERATIONS: meet and join
%--------------------------------------------------------------------

-spec join(pt(), pt()) -> ln().
% @doc
% join two DISTINCT points to get a line

join(Equal, Equal) ->
    error(badarg);
join({pt, X1, Y1}, {pt, X2, Y2}) ->
    % The answer is:
    %
    %   ln(Y1 - Y2, X2 - X1, X1Y2 - X2Y1).
    %
    % Is incident with {X1, Y1}:
    %
    %     X1*(Y1 - Y2) + Y1(X2 - X1) + (X1Y2 - X2Y1)
    %   = X1Y1 - X1Y2 + Y1X2 - Y1X1 + X1Y2 - X2Y1
    %   = 0
    %
    % Is incident with {X2, Y2}:
    %
    %     X2*(Y1 - Y2) + Y2(X2 - X1) + (X1Y2 - X2Y1)
    %   = X2Y1 - X2Y2 + X2Y2 - X1Y2 + X1Y2 - X2Y1
    %   = 0
    %
    % Given two distinct points, there is exactly one line incident
    % with both of them
    ln(qx_q:minus(Y1, Y2),
       qx_q:minus(X2, X1),
       qx_q:minus(qx_q:times(X1, Y2),
                  qx_q:times(X2, Y1))).



-spec meet(ln(), ln()) -> pt().
% @doc
% Meet two lines that are NOT PARALLEL to get a point.

meet(L1, L2) ->
    Transverse = not parallel(L1, L2),
    case Transverse of
        true  -> really_meet(L1, L2);
        false -> error(badarg)
    end.

really_meet({ln, A1, B1, C1}, {ln, A2, B2, C2}) ->
    % This has a sign error. check against the scheme implementation.
    % going to bed.
    %
    % We are solving the matrix equation
    %
    %   [A1 B1][X] = [-C1]
    %   [A2 B2][Y]   [-C2]
    %
    % The answer will be the inverse of the matrix, times [-C1 -C2]^t
    %
    % The inverse of the matrix is
    %
    % The inverse of the matrix is
    %
    %  (1 / (A1B2 - B1A2)) * [ B2 -B1]
    %                        [-A2  A1]
    %
    % So when the dust settles, we get
    %
    %   X = ( B1*C2 - B2*C1)/Det
    %   Y = (-A1*C2 + A2*C1)/Det
    %   Y = ( A2*C1 - A1*C2)/Det
    Det = qx_q:minus(qx_q:times(A1, B2),
                     qx_q:times(B1, A2)),
    X = qx_q:divide(qx_q:minus(qx_q:times(B1, C2), qx_q:times(B2, C1)),
                    Det),
    Y = qx_q:divide(qx_q:minus(qx_q:times(A2, C1), qx_q:times(A1, C2)),
                    Det),
    pt(X, Y).



%--------------------------------------------------------------------
% TESTS
%--------------------------------------------------------------------

-spec incident
        (pt(), ln()) -> boolean();
        (ln(), pt()) -> boolean().

incident(Ln = #ln{}, Pt = #pt{}) ->
    incident(Pt, Ln);
incident({pt, X, Y}, {ln, A, B, C}) ->
    Zero = zero(),
    IP = qx_q:plus([qx_q:times(A, X),
                    qx_q:times(B, Y),
                    C]),
    Result = IP =:= Zero,
    Result.



-spec parallel(ln(), ln()) -> boolean().

parallel(L1, L2) ->
    Zero = zero(),
    Spread = spread(L1, L2),
    Result = Zero =:= Spread,
    Result.



-spec perpendicular(ln(), ln()) -> boolean().

perpendicular(L1, L2) ->
    One    = one(),
    Spread = spread(L1, L2),
    Result = One =:= Spread,
    Result.



%--------------------------------------------------------------------
% STATISTICS
%--------------------------------------------------------------------

-spec quadrance(pt()) -> q().

quadrance(P) ->
    quadrance(origin(), P).



-spec quadrance(pt(), pt()) -> q().

quadrance({pt, X1, Y1}, {pt, X2, Y2}) ->
    % (X1 - X2)^2 + (Y1 - Y2)^2
    %
    % As soon as I've introduced Pythagorean quadrance, I've
    % implicitly imposed a notion of "right angle"
    %
    % It's worth noting that we can define "parallelism" without
    % right angles, but "perpendicularity" obviously implies a notion
    % of "right angle", even if we aren't using angles.
    qx_q:plus(qx_q:sq(qx_q:minus(X1, X2)),
              qx_q:sq(qx_q:minus(Y1, Y2))).



-spec spread(ln(), ln()) -> q().

spread(L1, L2) ->
    One = one(),
    Cross = cross(L1, L2),
    qx_q:minus(One, Cross).



-spec cross(ln(), ln()) -> q().

% this may have an error in it, check after sleep and compare against
% scheme implementation
cross({ln, A1, B1, _}, {ln, A2, B2, _}) ->
    % start with the law of cosines from CIA trigonometry
    %
    % V.W = |V||W|cos(Theta)
    % cos(Theta) =   V . W
    %              ---------
    %               |V| |W|
    %
    % If we square this, we get the "cross"
    % So the cross is the dot squared divided by the product
    % of the quadrances
    Dot = qx_q:plus(qx_q:times(A1, A2), qx_q:times(B1, B2)),
    Q1  = qx_q:plus(qx_q:times(A1, A1), qx_q:times(B1, B1)),
    Q2  = qx_q:plus(qx_q:times(A2, A2), qx_q:times(B2, B2)),
    Res = qx_q:divide(qx_q:times(Dot, Dot),
                      qx_q:times(Q1 , Q2 )),
    Res.


%====================================================================
% INTERNALS
%====================================================================

zero() -> qx_q:zero().
one() -> qx_q:one().
