% @doc qx_q = Qanal Xanal Quotients [= rationals]
-module(qx_q).

-export_type([q/0]).
-export([bot/1,
         divide/2,
         from_z/1,
         minus/1,
         minus/2,
         one/0,
         oneover/1,
         plus/1,
         plus/2,
         q/2,
         sq/1,
         times/1,
         times/2,
         top/1,
         to_z/1,
         zero/0]).

-type z()   :: qx_z:z().
%-type znn() :: qx_z:znn().
-type zp()  :: qx_z:zp().
-type q()   :: {q, z(), zp()}.

%%% api

-spec bot(q()) -> zp().
% @doc returns the bottom of a fraction.

bot({q, _, B}) ->
    B.



-spec divide(q(), q()) -> q().
% @doc divide the first argument by the second

divide(A, B) ->
    times(A, oneover(B)).



-spec from_z(z()) -> q().
% @doc converts an integer to a fraction


from_z(Z) when is_integer(Z) ->
    q(Z, 1).



-spec minus(q()) -> q().
% @doc returns additive inverse of rational

minus({q, T, B}) ->
    q(-1 * T, B).



-spec minus(q(), q()) -> q().
% @doc subtracts the second argument from the first

minus(A, B) ->
    plus(A, minus(B)).



-spec one() -> q().
% @doc the multiplicative identity with rationals

one() ->
    q(1, 1).



-spec oneover(q()) -> q().
% @doc multiplicative inverse

oneover({q, T, B}) ->
    q(B, T).



-spec plus([q()]) -> q().
% @doc add a list of rationals

plus(List) ->
    plus_accum(List, zero()).



-spec plus(q(), q()) -> q().
% @doc add two rationals

plus({q, TL, BL}, {q, TR, BR}) ->
    % make common denominator
    q((TL * BR) + (TR * BL),
      BR * BL).



-spec q(z(), z()) -> q().
% @doc form the quotient of two integers

q(_, 0) ->
    error(badarg);
q(N, 1) ->
    {q, N, 1};
q(Top, Bot) when Bot < 0 ->
    q(-1 * Top, -1 * Bot);
q(Top, Bot) when 0 < Bot,
                 is_integer(Top),
                 is_integer(Bot) ->
    GCD = qx_z:gcd(Top, Bot),
    {q, Top div GCD,
        Bot div GCD}.



-spec sq(q()) -> q().
% @doc square a rational

sq(Q) ->
    times(Q, Q).


-spec times([q()]) -> q().
% @doc product of a list of rationals; short circuits on 0

times(Qs) ->
    times_accum(Qs, one()).



-spec times(q(), q()) -> q().
% @doc product of two rationals

times({q, TL, BL}, {q, TR, BR}) ->
    q(TL * TR,
      BL * BR).



-spec to_z({q, z(), 1}) -> z().
% @doc convert a fraction where the denominator is 1 to an integer,
% crash otherwise

to_z({q, Z, 1}) -> Z.



-spec top(q()) -> z().
% @doc returns the top of a fraction.

top({q, T, _}) ->
    T.



-spec zero() -> q().
% @doc the additive identity with rationals

zero() ->
    q(0, 1).



%%% internals

-spec plus_accum(List :: [q()], AccumSum :: q()) -> Sum :: q().
% @private
% plus with accumulator

plus_accum([], Accum) ->
    Accum;
plus_accum([X | Xs], Accum) ->
    plus_accum(Xs, plus(Accum, X)).



-spec times_accum(List :: [q()], AccumProd :: q()) -> Prod :: q().
% @private
% times with accumulator; will short-circuit if accumulator is 0

times_accum(_, Zero = {q, 0, 1}) ->
    Zero;
times_accum([], Accum) ->
    Accum;
times_accum([X | Xs], Accum) ->
    times_accum(Xs, times(Accum, X)).
