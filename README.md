Lines.

Most people think of a line in [slope-intercept form]

  y = Mx + B

where y is viewed as a function of x. In this case,

  - M is the [slope] of the line
  - B is the [y-intercept] of the line, which is the y-coordinate
    at which the line intersects the vertical axis.

This captures [almost all] lines; the lines it doesn't capture are
vertical lines given by

  x = const

because in that case, y is no longer a single-valued function of x.
The [standard form] for a line is a line of the form

      Ax + By + C = 0

Standard form captures all possible lines.  A simple algebraic
manipulation of a line in slope-intercept form can yield a line in
standard form, via

  -Mx + y - B = 0

However, notice that this equation is also true if we multiply both
sides of the equation by any constant. Therefore we have a unique
representation issue. The solution is to represent a line as a
[proportion], and to have a rule for canonical representations of
proportions.

A [line] is defined as a triple {A, B, C} where at least one of A
and B is nonzero. This constraint is to eliminate trivial cases

If both A and B are zero, we have one of two cases:

  1. Case C =:= 0, we have the equation

          0 = 0.

     The subset of the plane for which 0 =:= 0 is the entire plane,
     which is not a "line" in the usual sense.

  2. Case C =/= 0, we have the equation

          SomethingThatIsNotZero = 0,

     The subset of the plane for which e.g. 3 = 0 is the empty set,
     which again is not a "line"

Therefore we insist that at least one of A and B is not zero.

Such a triple {A, B, C} is called a [proportion]. Two triples
are equal if one is a nonzero constant multiple of the other.

So we run into a similar unique representation problem as with
fractions
